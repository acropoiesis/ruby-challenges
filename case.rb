fav_color = "purple"

case fav_color
when 'red'
  puts "Red like fire!"
when 'orange'
  puts "Orange like, well... an orange."
when 'yellow'
  puts "Yellow daffodils are so pretty in the string!"
when 'green'
  puts "Have you been to the Emerald City in Oz?"
when 'blue'
  puts "Blue like the sky"
when 'purple'
  puts "Purple plums are the tastiest."
else
  puts "Hmm, well I don't know what that color is!"
end

weather = "sunny"

case weather
when 'sunny'
  puts "It's sunny, but is winter, so it's probably pretty cold outside today!"
when 'cloudy'
  puts "Check the weather, for it may be mild today."
when 'windy'
  puts "Don't go out!"
else
  puts "Check your weather app."
end
