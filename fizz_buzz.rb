def count(from, to)
	counter = from
	while (counter <= to)
		if (counter % 15 == 0)
			puts "FizzBuzz"
		elsif (counter % 3 == 0)
			puts "Fizz"
		elsif (counter % 5 == 0)
			puts "Buzz"
		else
			puts counter
		end
		counter += 1
	end
end
			
count(1, 100)